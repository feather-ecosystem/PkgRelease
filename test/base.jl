using Test, SafeTestsets

@safetestset "Base utilities" begin

    using PkgRelease
    using PkgRelease: bump, Patch, Minor, Major

    @testset "bump version" begin
        @test bump(v"1.1.1", Patch()) == v"1.1.2"
        @test bump(v"1.1.1", Minor()) == v"1.2.0"
        @test bump(v"1.1.1", Major()) == v"2.0.0"
    end
end
