using Test

tests = [
            "base",
            "project"
            # "gitcmd",
            # "project",
            # "register"
        ]

@testset "PkgRelease" begin
    for t in tests
        fp = joinpath(dirname(@__FILE__), "$t.jl")
        println("$fp ...")
        include(fp)
    end
end # @testset
