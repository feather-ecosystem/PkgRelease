using Test, SafeTestsets

@safetestset "Project details" begin

    using PkgRelease
    using PkgRelease: get_project_field, set_project_field
    using PkgRelease: Patch, bump, increment_version_number

    # tests are run on Project.toml in the main directory
    cd(joinpath(@__DIR__,"..")) do

        @testset "project details" begin
                @test get_project_field("name") == "PkgRelease"
                @test get_project_field("uuid") == "94fffbe3-359b-419f-9c91-09df7dc1a4cb"
        end

        @testset "change project details" begin
            set_project_field("name", "\"Test\"")
            @test get_project_field("name") == "Test"
            set_project_field("name", "\"PkgRelease\"")
            @test get_project_field("name") == "PkgRelease"

            version = get_project_field("version")
            set_project_field("version", "\"2.2.2\"")
            @test get_project_field("version") == "2.2.2"
            set_project_field("version", "\"$version\"")
            @test get_project_field("version") == version
        end

        @testset "increment version" begin
            version = get_project_field("version")
            new_version = string(bump(VersionNumber(version), Patch()))

            increment_version_number(Patch())
            @test get_project_field("version") == new_version
            set_project_field("version", "\"$version\"")
            @test get_project_field("version") == version
        end
    end
end
