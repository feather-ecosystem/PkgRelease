export push_changes

function git_add_all()::Bool
    println("Adding changes...")
    try
        return success(`git add .`)
    catch
        println("Could not add changes.")
        false
    end
end

function git_commit(message)::Bool
    println("Commiting changes...")
    try
        return success(`git commit -m "$message"`)
    catch
        println("Could not commit changes.")
        false
    end
end

function git_tag(version, message)::Bool
    println("Tagging version...")
    try
        return success(`git tag -a "$version" -m "$message"`)
    catch
        println("Could not tag version.")
        false
    end
end

function git_push(options::String="")::Bool
    println("Pushing changes...")
    try
        if isempty(options)
            run(`git push`)
            return true
        else
            run(`git push $options`)
            return true
        end
    catch
        println("Could not push changes.")
        false
    end
end

function git_pull()::Bool
    println("Pulling changes...")
    try
        return succes(`git pull`)
    catch
        println("Could not pull changes.")
        false
    end
end