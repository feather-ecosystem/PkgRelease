export release

"""
    release(PkgRelease.Major)
    release(PkgRelease.Minor)
    release(PkgRelease.Patch)

Run pipeline that determines new git tags of a release, registers new version
to the private registry, pushes changes of the package and registry to the
remote repository.
"""
function release(::Type{S}, registry="FeatherRegistry") where {S<:VersionIncrement}
    return release_imp(S(), registry)
end

function release_imp(increment::VersionIncrement, registry::String)
    Pkg.activate(".")
    if testsuite_passes()
        new_version = increment_version_number(increment)
        println("New version: $new_version")
        
        tag_new_version(new_version)
        register_new_release(registry)      
        registry_tag_and_push_changes(increment, registry)
        println("New version succesfully released.")
    end
end

function register_new_release(registry="FeatherRegistry")
    registry_path = joinpath(DEPOT_PATH[1], "registries", registry);
    CurrentProject = get_project_field("name")

    println("Trying to add new release to the registry")
    eval(
        quote
            Pkg.activate() # enter global workspace

            using LocalRegistry
            
            register($CurrentProject; registry=$registry_path)
        end)
    println("Added new release to the registry.")
end

function registry_tag_and_push_changes(increment::VersionIncrement, registry::String)
    registry_path = joinpath(DEPOT_PATH[1], "registries", registry);
    cd(registry_path) do
        old_version = chomp(read(`git describe --abbrev=0`, String))
        new_version = string(bump(VersionNumber(old_version), increment))
        git_pull()
        git_push()
        git_tag("v$new_version", "Increased version => v$new_version")
        git_push("--tags")
        println("tagged a pushed a new version of the registry to the GitLab repository.")
    end
end
