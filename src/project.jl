using Pkg

function get_project_field(field::String)
    m = length(field)
    open("Project.toml") do file
        for line in eachline(file)
            if occursin("$field = ",line)
                return line[m+5:end-1]
            end
        end
    end
end

function set_project_field(field::String, value)
    m = length(field)
    (tmppath, tmpio) = mktemp()
    open("Project.toml") do file
        for line in  eachline(file, keep=true)
            if occursin("$field = ",line)
                line = replace(line, line[m+4:end-1] => value, count=1)
            end
            write(tmpio, line)
        end
    end
    close(tmpio)
    mv(tmppath, "Project.toml", force=true)
end

function increment_version_number(v_inc::VersionIncrement)
    old_version = VersionNumber(get_project_field("version"))
    new_version = bump(old_version, v_inc)
    str = string(new_version)
    set_project_field("version", "\"$str\"")
    return new_version
end

function testsuite_passes()::Bool
    return try
        println("Running test-suite.")
        Pkg.test();
        return true
    catch
        println("Test-suite failed.")
        false
    end
end

function tag_new_version(v::VersionNumber)::Bool
    version = string(v)
    message = "Increased version => v$version"
    if !git_pull() && git_add_all() && git_commit(message) && git_push()
        git_tag("v$version", message)
        git_push("--tags")
        println("tagged a pushed a new version of the package to the GitLab repository.")
        return true
    else
        error("Could not push version to package repository! Maybe you need to pull from remote first?")
    end
    return false
end
