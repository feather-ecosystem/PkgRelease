export get_environement_variable

abstract type VersionIncrement end

struct Major <: VersionIncrement end
struct Minor <: VersionIncrement end
struct Patch <: VersionIncrement end

function bump(v::VersionNumber, ::Patch)
    return VersionNumber(v.major, v.minor, v.patch+1, v.prerelease)
end

function bump(v::VersionNumber, ::Minor)
    return VersionNumber(v.major, v.minor+1, 0, v.prerelease)
end

function bump(v::VersionNumber, ::Major)
    return VersionNumber(v.major+1, 0, 0, v.prerelease)
end

get_environement_variable(variable) = ENV["$variable"]