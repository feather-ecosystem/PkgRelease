module PkgRelease

    using Pkg, LocalRegistry

    include("gitcmd.jl")
    include("base.jl")
    include("project.jl")
    include("register.jl")

end # module
