# PkgRelease

[![pipeline status](https://gitlab.com/feather-ecosystem/PkgRelease/badges/master/pipeline.svg)](https://gitlab.com/feather-ecosystem/PkgRelease/-/commits/master)
[![coverage report](https://gitlab.com/feather-ecosystem/PkgRelease/badges/master/coverage.svg)](https://gitlab.com/feather-ecosystem/PkgRelease/-/commits/master)
[![codecov](https://codecov.io/gl/feather-ecosystem/PkgRelease/branch/master/graph/badge.svg?token=3n8TRiZ6Mb)](https://codecov.io/gl/feather-ecosystem/PkgRelease)
[![Gitter](https://badges.gitter.im/feather-ecosystem/community.svg)](https://gitter.im/feather-ecosystem/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

This package automates the process of registering (a new version of) your package to the private repository.

## Getting started
If ssh-keys have been properly set then `PkgRelease.jl` is simply downloaded, installed and ready for use by the following commands
```
julia> Pkg.add("PkgRelease");

julia> using PkgRelease
```

## Releasing your package
Releasing the first version or a new version of your package is simple. First activate the project you wish to release
```
julia> using Pkg
julia> Pkg.activate()
julia> Pkg.develop("MyPackage")
julia> Pkg.activate("MyPackage")
```
Subsequently, following the rules of [SemVer](https://semver.org/), choose to release one of the following: a `Patch`
```
julia> release(PkgRelease.Patch)
```
a `Minor` release
```
julia> release(PkgRelease.Minor)
```
or a `Major` release
```
julia> release(PkgRelease.Major)
```

## Implementation
The following steps are performed automatically by `PkgRelease`
1. Increments the version number in your `Project.toml`
2. Registers your package to the registry using [LocalRegistry.jl](https://github.com/GunnarFarneback/LocalRegistry.jl)
3. Generates git tags of the package and the rivate registry and pushes all changes to the remote repository.
